#pragma once
//
// Created by Ofir Ben Yosef on 08/09/2021.
//

#ifndef MY_APPLICATION_SIGNAL_PROCESSING_H
#define MY_APPLICATION_SIGNAL_PROCESSING_H


#include "parameters.h"
#include <string.h>
#include <iostream>
#include <ctime>

// functions :
void bubbleSort(double arr[], int n);
int tone_detect(short* in, short* out, int length);
void copy_array(short* in, short* out, int length);
int find_symbol(double arr[]);
void bubbleSort2(double arr1[], double arr2[], int n);
void My_fft(short in_m[], double out_n[]);
void save_array(short arr1[], short arr2[]);
void push_array(short arr1[], short arr2[]);
char majority_vote_char(int symbols[], int len);
char sliding_windows_char(short* in, int length);
int tone_detect2(short in_m[], int length);
void decode_symbol(unsigned int in_arr[], unsigned int out_arr[], int hour, int minute);
void decode_shell(unsigned int in_arr[], unsigned int out_arr[], int flag);

#endif //MY_APPLICATION_SIGNAL_PROCESSING_H
