#define MyFunctions _declspec(dllexport)
#include "fft.h"
#include "parameters.h"
#include <iostream>
#include <stdio.h>
#include <ctime>




using namespace std;
//#include "MyFucntions.h"

extern "C" {
	MyFunctions void copy_array(double in_m[], double out_m[], int length) {
		for (int j = 0; j < length; j++) {
			out_m[j] = in_m[j];
		}
	}
    short symbol_freq_T[11][3] = {
        {   625,   3000,   7000},//0
        {  2500,   4500,   6000},//1
        {  1000,   5000,   7375},//2
        {  2000,   4500,   5625},//3
        {   625,   4000,   7375},//4
        {  1375,   3000,   6500},//5
        {  2500,   3375,   7000},//6
        {  1000,   4000,   5625},//7
        {  2000,   5000,   6500},//8
        {  1375,   3375,   6000},//9
        {  4000,   5000,   6000} };//@

    short symbol_freq_start_T[NUM_SYMBOLS][DETECTED_TONES] = {
         {   100,    180,    308},
         {   164,    228,    276},
         {   116,    244,    324},
         {   148,    228,    260},
         {   100,    212,    324},
         {   132,    180,    292},
         {   164,    196,    308},
         {   116,    212,    260},
         {   148,    244,    292},
         {   132,    196,    276},
         {   212,    244,    340},
         {   100,    228,    292},
         {   116,    196,    340},
         {   132,    164,    260},
         {   148,    276,    340},
         {   180,    308,    324},
         {   148,    196,    324}};

    short symbol_freq_end_T[NUM_SYMBOLS][DETECTED_TONES] = {
         {  3996,   3916,   3788},
         {  3932,   3868,   3820},
         {  3980,   3852,   3772},
         {  3948,   3868,   3836},
         {  3996,   3884,   3772},
         {  3964,   3916,   3804},
         {  3932,   3900,   3788},
         {  3980,   3884,   3836},
         {  3948,   3852,   3804},
         {  3964,   3900,   3820},
         {  3884,   3852,   3756},
         {  3996,   3868,   3804},
         {  3980,   3900,   3756},
         {  3964,   3932,   3836},
         {  3948,   3820,   3756},
         {  3916,   3788,   3772},
         {  3948,   3900,   3772}};

    MyFunctions void swap(double* xp, double* yp)
    {
        double temp = *xp;
        *xp = *yp;
        *yp = temp;
    }


    MyFunctions void bubbleSort2(double arr1[], double arr2[], int n) {
        /*
             * sort an array in descending order and index array at the same order
             */
        for (int i = n; i >= 0; i--)
            for (int j = n; j > n - i; j--)
                if (arr1[j] > arr1[j - 1]) {
                    swap(&arr1[j], &arr1[j - 1]);//main array
                    swap(&arr2[j], &arr2[j - 1]);//index array
                }
    }
    MyFunctions int find_symbol(double arr[]) {
        /*
         *@ param : arr[] - array of detected tones
         * if find symbol that fit return the symbol
         * else return -1
         */
        for (int i = 0; i < NUM_SYMBOLS; i++) {
            if ((arr[0] == symbol_freq_start_T[i][0]) && (arr[1] == symbol_freq_start_T[i][1]) && (arr[2] == symbol_freq_start_T[i][2])) {
                return i;
            }
        }
        return -1;
    }

    MyFunctions void bubbleSort(double arr[], int n)
    {
        int i, j;
        for (i = 0; i < n - 1; i++)

            // Last i elements are already in place
            for (j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1])
                    swap(&arr[j], &arr[j + 1]);
    }
    MyFunctions void My_fft(short in_m[],double out_m[]) {
        
        double SA[FFT_SIZE][2];
        double amp[FFT_SIZE] = { 0 };
        double inx_arr[FFT_SIZE] = { 0 };
        fft_init(MAX_FFT_SIZE);
        static int idnx[16] = { 160, 256, 352, 512, 640, 768, 864, 1024, 1152, 1280, 1440, 1536, 1664, 1792, 1888, 1984 };

        for (int j = 0; j < FFT_SIZE; j++) {

            SA[j][0] = (double)in_m[j];
            SA[j][1] = 0;
            inx_arr[j] = (double)j;
        }
        gpfft(SA, 0);
        for (int i = 0; i < FFT_SIZE; i++) {
       
            out_m[i] = SA[i][0] * SA[i][0] + SA[i][1] * SA[i][1];
        }



    }

    MyFunctions int tone_detect(short in_m[], int length) {
        double tones[3] = { 0 };
        double SA[FFT_SIZE][2];
        double amp[FFT_SIZE] = { 0 };

        double inx_arr[FFT_SIZE] = { 0 };
        static int idx[16] = { 160, 256, 352, 512, 640, 768, 864, 1024, 1152, 1280, 1440, 1536, 1664, 1792, 1888, 1984 };
        fft_init(MAX_FFT_SIZE);
        for (int j = 0; j < FFT_SIZE; j++) {

            SA[j][0] = (double)in_m[j];
            SA[j][1] = 0;
            inx_arr[j] = (double)j;
        }
        gpfft(SA, 0);
        for (int i = 0; i < 16; i++) {
                    amp[idx[i]] = SA[idx[i]][0] * SA[idx[i]][0] + SA[idx[i]][1] * SA[idx[i]][1];
        }
        
        bubbleSort2(amp, inx_arr, FFT_SIZE);
       /* if (!far_next_freq(amp)) {
            return -1;
          }
        */
        tones[0] = inx_arr[0] * SAMPLING_FREQ / FFT_SIZE;
        tones[1] = inx_arr[1] * SAMPLING_FREQ / FFT_SIZE;
        tones[2] = inx_arr[2] * SAMPLING_FREQ / FFT_SIZE;

        bubbleSort(tones, DETECTED_TONES);

        std::cout << inx_arr[0]<<',' << inx_arr[1] << ',' << inx_arr[2] << endl;


        return (int)find_symbol(tones);
    }

    MyFunctions int tone_detect2(short in_m[], int length) {
        double tones[3] = { 0 };
        double SA[FFT_SIZE][2];
        short SSA[BLOCK_SIZE * 2] = { 0 };
        double amp[FFT_SIZE] = { 0 };
        double inx_arr[FFT_SIZE] = { 0 };
        double amp_used_freq[NUM_OF_FREQ] = { 0 };
        double amp_used_freq_idx[NUM_OF_FREQ] = { 0 };

        for (int j = 0; j < FFT_SIZE; j++) {
            
            SA[j][0] = (double)in_m[j];
            SA[j][1] = 0;
            inx_arr[j] = (double)j;
        }
        gpfft(SA, 0);
        int step = 16;
        int start = 100;
        for (int i = 0; i < NUM_OF_FREQ; i++) {
            amp[start + i * step] = SA[start + i * step][0] * SA[start + i * step][0] + SA[start + i * step][1] * SA[start + i * step][1];
            amp_used_freq[i] = amp[start + i * step];
            amp_used_freq_idx[i] = start + i * step;
        }
      
        bubbleSort2(amp_used_freq, amp_used_freq_idx, NUM_OF_FREQ);
       
       if (amp_used_freq[3]>0.5*amp_used_freq[2]) {
            return -1;
       }
        
        tones[0] = amp_used_freq_idx[0];
        tones[1] = amp_used_freq_idx[1];
        tones[2] = amp_used_freq_idx[2];

        bubbleSort(tones, DETECTED_TONES);
     
        return (int)find_symbol(tones);
    }

    MyFunctions void singen_fft(short* out_m, int length, short symbol) {
        double sig_fft[FFT_SIZE][2] = { {0} };
        for (int i = 0; i < TONE_DETECTION; i++) {
            sig_fft[symbol_freq_start_T[symbol][i]][1] = -(FFT_SIZE / 6) * 1;
            sig_fft[symbol_freq_end_T[symbol][i]][1] = (FFT_SIZE / 6) * 1;
        }
        ifft(sig_fft, 0);
        for (int i = 0; i < (length / 2); i++) {
            out_m[2 * i] = (short)(Q14 * sig_fft[i][0]);
            out_m[2 * i + 1] = (short)(Q14 * sig_fft[i][0]);
        }
    }

    MyFunctions void save_array(short arr1[], short arr2[]) {
        errno_t err;
        FILE* pFile;
        short int a[] = { 1234 };
        err = fopen_s(&pFile, "c://temp//audio.dat", "ab+");
        if (pFile == nullptr) {
            return;
        }
        fwrite(arr1, FFT_SIZE, sizeof(short int), pFile);
        fclose(pFile);
        return;


    }


    MyFunctions void push_array(short arr1[], short arr2[]) {
        /*
         * arr1[] - new array size of FFT_SIZE
         * arr2[] old array size of 2*FFT_SIZE
         * */
        short temp = 0;
        for (int i = 0; i < FFT_SIZE; i++) {
            temp = arr2[i + FFT_SIZE];
            arr2[i + FFT_SIZE] = arr1[i];
            arr2[i] = temp;
        }
    }

    MyFunctions char majority_vote_char(int symbols[], int len) {
        int count[NUM_SYMBOLS] = { 0 };
        char symbol_chars[NUM_SYMBOLS] = { '0','1','2','3','4','5','6','7','8','9','@','#','$','!','%','*','N'};
        char not_found = 'X';
        for (int i = 0; i < len; i++) {
            if (symbols[i] == -1) {
                continue;
            }
            else {
                count[symbols[i]]++;
            }
        }
        for (int i = 0; i < NUM_SYMBOLS; i++) {
            if (count[i] > 2) {
                return symbol_chars[i];
            }
        }
        return not_found;
    }
    static short buffer[2 * FFT_SIZE] = { 0 };
    MyFunctions char sliding_windows_char(short* in, int length) {

        short SSA[FFT_SIZE] = { 0 };
        
        int k = 0;
        save_array(in, buffer);
        push_array(in, buffer);
        int add = FFT_SIZE / 4; //1024
        int the_symbols[5] = { 0 };
        for (int i = 0; i <= FFT_SIZE; i = i + add) {
            for (int j = 0; j < FFT_SIZE; j++) {
                SSA[j] = buffer[j + i];
            }
            the_symbols[k] = tone_detect2(SSA, FFT_SIZE);
            
            k++;
        }
        return majority_vote_char(the_symbols, 5);
    }


    MyFunctions void decode_symbol(unsigned int in_arr[], unsigned int out_arr[], int hour, int minute) {
        const unsigned int mask = 15;
        // take only one digit
        unsigned int h_1 = hour / 10;
        unsigned int h_2 = hour % 10;
        unsigned int m_1 = minute / 10;
        unsigned int m_2 = minute % 10;
        // fill out
        out_arr[0] = ~((h_1 & mask) ^ ((~in_arr[0]) & mask)) & mask;
        out_arr[1] = ~((h_2 & mask) ^ ((~in_arr[1]) & mask)) & mask;
        out_arr[2] = ~((m_1 & mask) ^ ((~in_arr[2]) & mask)) & mask;
        out_arr[3] = ~((m_2 & mask) ^ ((~in_arr[3]) & mask)) & mask;
        return;
    }

    MyFunctions void decode_shell(unsigned int in_arr[], unsigned int out_arr[], int flag) {
        time_t now = time(0);
        struct tm buff;
        localtime_s(&buff, &now);
        int hour = buff.tm_hour;
        int minute = buff.tm_min;
        if (flag == 0) {//before
            if (minute == 0) {
                decode_symbol(in_arr, out_arr, hour - 1, 59);
            }
            else {
                decode_symbol(in_arr, out_arr, hour, ((minute - 1) % 60));
            }

            cout << flag << '.' << out_arr[0] << out_arr[1] << out_arr[2] << out_arr[3] << endl;
        }
        else if (flag == 1) {

            decode_symbol(in_arr, out_arr, hour, minute);
            cout << flag << '.' << out_arr[0] << out_arr[1] << out_arr[2] << out_arr[3] << endl;

        }
        else if (flag == 2) {//after

            if (minute == 59) {
                decode_symbol(in_arr, out_arr, hour +1 , ((minute + 1) % 60));
            }
            else {
                decode_symbol(in_arr, out_arr, hour, ((minute + 1) % 60));
            }
            
            cout << flag << '.' << out_arr[0] << out_arr[1] << out_arr[2] << out_arr[3] << endl;

        }
    }


}

