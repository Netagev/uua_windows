#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "parameters.h"
#include "fft.h"
double wc[MAX_FFT_SIZE];
double ws[MAX_FFT_SIZE];

void fft_init(int fftsize) {
	int i;


	for (i = 0; i < fftsize; i++) {	    // set up twiddle constants in w
		wc[i] = (double)cos(2 * PI * i / fftsize);
		ws[i] = (double)(-sin(2 * PI * i / fftsize));
	}
}

void gpfft(double Y[][2], int fftind) {
	int fftsize[] = { FFT_SIZE, MAX_FFT_SIZE };
	double temp1[2], temp2[2];             //temporary storage variables
	int i, j, k;                       //loop counter variables
	int upper_leg, lower_leg;   	    //index of upper/lower butterfly leg
	int index;
	float Sig_en = 0;                 //index/step through twiddle constant
	static short int steptbl[] = { 16, 1 }; // {MAX_FFT_SIZE/FFT_SIZE, MAX_FFT_SIZE}
	static short int num_stages[] = { 12, 16 }; // {log2(FFT_SIZE), log2(MAX_FFT_SIZE)}
	int step = steptbl[fftind];
	int	leg_diff = fftsize[fftind] / 2;


	for (i = 0; i < num_stages[fftind]; i++)
	{
		index = 0;
		for (j = 0; j < leg_diff; j++)
		{
			for (upper_leg = j; upper_leg < fftsize[fftind]; upper_leg += (2 * leg_diff))
			{
				lower_leg = upper_leg + leg_diff;
				temp1[0] = ((Y[upper_leg])[0] + (Y[lower_leg])[0]);
				temp1[1] = ((Y[upper_leg])[1] + (Y[lower_leg])[1]);
				temp2[0] = ((Y[upper_leg])[0] - (Y[lower_leg])[0]);
				temp2[1] = ((Y[upper_leg])[1] - (Y[lower_leg])[1]);


				temp1[0] /= 2;
				temp1[1] /= 2;
				temp2[0] /= 2;
				temp2[1] /= 2;


				(Y[lower_leg])[0] = (((temp2[0] * (wc[index]))
					- (temp2[1] * (ws[index]))));
				(Y[lower_leg])[1] = (((temp2[0] * (ws[index]))
					+ (temp2[1] * (wc[index]))));
				(Y[upper_leg])[0] = temp1[0];
				(Y[upper_leg])[1] = temp1[1];
			}
			index += step;
		}
		leg_diff = leg_diff / 2;
		step *= 2;
	}
	j = 0;
	for (i = 1; i < (fftsize[fftind] - 1); i++)     //bit reversal for resequencing data
	{
		k = fftsize[fftind] / 2;
		while (k <= j)
		{
			j = j - k;
			k = k / 2;
		}
		j = j + k;
		if (i < j)
		{
			temp1[0] = (Y[j])[0];
			temp1[1] = (Y[j])[1];
			(Y[j])[0] = (Y[i])[0];
			(Y[j])[1] = (Y[i])[1];
			(Y[i])[0] = temp1[0];
			(Y[i])[1] = temp1[1];
		}
	}
}

void gpfft_dual(double Y1[][2], double Y2[][2]) {
	int j;
	double temp[4];
	gpfft(Y1, FFT64K);
	/*	SIG1=zeros(FFT_SIZE,1);
		SIG1(1)=real(V(1));
		SIG1(FFT_SIZE/2+1)=real(V(FFT_SIZE/2+1));
		SIG1(2:FFT_SIZE/2)=(real(V(2:FFT_SIZE/2))+real(V(end:-1:FFT_SIZE/2+2)))/2 ...
			+ 1i*(imag(V(2:FFT_SIZE/2))-imag(V(end:-1:FFT_SIZE/2+2)))/2;

		SIG2=zeros(FFT_SIZE,1);
		SIG2(1)=imag(V(1));
		SIG2(FFT_SIZE/2+1)=imag(V(FFT_SIZE/2+1));
		SIG2(2:FFT_SIZE/2)=(imag(V(2:FFT_SIZE/2))+imag(V(end:-1:FFT_SIZE/2+2)))/2 ...
			+ 1i*(-real(V(2:FFT_SIZE/2))+real(V(end:-1:FFT_SIZE/2+2)))/2; */

	Y2[0][0] = Y1[0][1];

	for (j = 1; j < (FFT_SIZE / 2); j++) {
		temp[0] = (Y1[j][0] + Y1[FFT_SIZE - j][0]) / 2;
		temp[1] = (Y1[j][1] - Y1[FFT_SIZE - j][1]) / 2;
		temp[2] = (Y1[j][1] + Y1[FFT_SIZE - j][1]) / 2;
		temp[3] = (-Y1[j][0] + Y1[FFT_SIZE - j][0]) / 2;

		Y1[j][0] = temp[0];
		Y1[j][1] = temp[1];
		Y2[j][0] = temp[2];
		Y2[j][1] = temp[3];
	}

	for (j = 1; j < (FFT_SIZE / 2); j++) {
		Y1[FFT_SIZE - j][0] = Y1[j][0];
		Y1[FFT_SIZE - j][1] = -Y1[j][1];
		Y2[FFT_SIZE - j][0] = Y2[j][0];
		Y2[FFT_SIZE - j][1] = -Y2[j][1];
	}
}


void ifft(double Y[][2], int fftind) {
	int i = 0;
	int fftsize[] = { FFT_SIZE, MAX_FFT_SIZE };
	static short int num_stages[] = { 12, 16 }; // {log2(FFT_SIZE), log2(MAX_FFT_SIZE)}
	for (i = 0; i < fftsize[fftind]; i++) {
		Y[i][1] = -Y[i][1];
	}

	gpfft(Y, fftind);

	for (i = 0; i < fftsize[fftind]; i++) {
		Y[i][1] = -Y[i][1];
	}


}