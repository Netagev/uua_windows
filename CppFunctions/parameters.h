#pragma once


#ifndef EWALLET_PARAMETERS_H
#define EWALLET_PARAMETERS_H

/* ---------------------------------------------------- */
// C simulation definitions

// processing modes 
#define LOOPBACK			0
#define SINGEN				1
#define SPECTRAL_ANALYSIS	2
#define TONE_DETECTION		3
#define INV_FFT				4
#define CODE				5
#define DECODE				6
#define MAJORITY_VOTING		7
#define ENCODE_MESSAGE      8
#define DECODE_MESSAGE      9
#define INPUT_INSERTION     10
/* ---------------------------------------------------- */


/* ---------------------------------------------------- */
// Define sampling freq for C simulation
// Note: in Anadroid Studio the sampling freq is defined in Java
#define SAMPLING_FREQ		32000
#define END                 254
#define START               184
/* ---------------------------------------------------- */


/* ---------------------------------------------------- */
// Definitons common to C simulation and Android Native 

#define PI					3.14159265358979323846
#define Q14                 16384

#define FFT_SIZE			4096
#define MAX_FFT_SIZE		65536
#define MIN_FFT_SIZE        128
#define BLOCK_SIZE			4096
#define DETECTED_TONES		3
#define START_TONE          256
#define END_TONE            768
#define JUMP				32
#define NUM_SYMBOLS         17
#define NUM_OF_FREQ			16



/* ---------------------------------------------------- */


#endif //EWALLET_PARAMETERS_H
