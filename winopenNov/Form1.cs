﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Numerics;
using Microsoft.Win32;
using Microsoft.VisualBasic;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using NAudio.CoreAudioApi;
using NAudio.Utils;
using System.Timers;
using ScottPlot.WinForms;
using ScottPlot;



namespace winopenNov
{

    public partial class Form1 : Form
    {
        public const string CppFunctionsDLL = @"..\..\..\x64\Debug\CppFunctions.dll";
        //public const string CppFunctionsDLL = @"cpp\CppFunctions.dll";  //for exe run


        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void copy_array(double[] in_m , double[] out_m, int length);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]

        public static extern int tone_detect(short[] in_m, int length);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]

        public static extern void singen_fft(short[] out_m, int length, short symbol);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]

        public static extern void  My_fft(short[] in_m, double[] out_m);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern int find_symbol(double[] arr);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]

        public static extern char sliding_windows_char(short[] in_m, int length);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]

        public static extern int tone_detect2(short[] in_m, int length);

        [DllImport(CppFunctionsDLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void decode_shell(int[] in_arr, int[] out_arr, int flag);




        // MICROPHONE ANALYSIS SETTINGS
        private int RATE = 32000; // sample rate of the sound card
        private int BUFFERSIZE = (int)Math.Pow(2, 13); // must be a multiple of 2 size :/8196
        private int Q14 = 16384;
        private int DETECTED_TONES = 3;
        private int FFT_SIZE = 4096;
        
        // prepare class objects
        public BufferedWaveProvider bwp;
        public readonly object balanceLock = new object();
        public int x_counter = 0;
        public string user_pin;
        public string user_pin_enter;
        public string lock_pin;
        public string delay_lock_pin;
        public bool delay_lock_flag = false;
        public bool delete_pin_flag = false;
        public int[] lockint = { 0, 0, 0, 0 };
        public int[] delay_lockint = { 0, 0, 0, 0 };
        public char[] symbol_chars = new char[17] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '@', '#', '$', '!', '%', '*', 'N' };
        private int[] pin_int = new int[4];
        private int pos_in_pin = 0;
        private char prev_symbol;//private int prev_symbol;
        private char[] pin = new char[4];//private int[] pin = new int[4];
        private int[] decoded_pin = new int[4];
        private int[] decoded_pin_before = new int[4];
        private int[] decoded_pin_after = new int[4];

        


        public Form1()
        {

            InitializeComponent();
            SetupGraphLabels();
            locking();  // first- screen lock

            StartListeningToMicrophone();
            timer1.Enabled = true;
            timer1.Start();



            // AutoScaleToolStripMenuItem_Click();
            File.WriteAllText("buf_audio_in.txt", "~~~~~start~~~~~~\n");
            File.WriteAllText("buf_audio_in.csv", "~~~~~start~~~~~~\n");



        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
            // save the initial user's password 
            user_pin= textBox1.Text;
          
           
        }

        private void label1_Click(object sender, EventArgs e)
        {
            unlocking();
        }
        private void locking(){

           FormBorderStyle = FormBorderStyle.None;
           WindowState = FormWindowState.Maximized;
           TopMost = true;
        }
        private void unlocking()
        {
           var t = Task.Run(async delegate
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                return 43;
            });
            t.Wait();
            Console.WriteLine("Task t Status: {0}, Result: {1}",
            t.Status, t.Result);


            FormBorderStyle = FormBorderStyle.Sizable;
            WindowState = FormWindowState.Minimized;
            TopMost = false;
            enter_msg.Text = "";
            textBox1.Clear();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            unlocking();
        }
        
        void AudioDataAvailable(object sender, WaveInEventArgs e)
        {
            lock (balanceLock)
            {
                bwp.AddSamples(e.Buffer, 0, e.BytesRecorded);
            }
        }

        public void SetupGraphLabels()
        {
            formsPlot1.Plot.Title("Microphone PCM Data");
            formsPlot1.Plot.XLabel("Time (ms)");
            formsPlot1.Plot.YLabel("Amplitude (PCM)");
            formsPlot1.Refresh();

            formsPlot2.Plot.Title("Microphone FFT Data");
            formsPlot2.Plot.YLabel("Power (raw)");
            formsPlot2.Plot.XLabel("Frequency (Hz)");
            formsPlot2.Refresh();
          
        }
        public void StartListeningToMicrophone(int audioDeviceNumber = 0)
        {
            WaveIn wi = new WaveIn
            {
                DeviceNumber = audioDeviceNumber,
                WaveFormat = new NAudio.Wave.WaveFormat(RATE, 1),
                BufferMilliseconds = (int)((double)BUFFERSIZE / (double)RATE * 1000.0)
            };
            wi.DataAvailable += new EventHandler<WaveInEventArgs>(AudioDataAvailable);
            lock (balanceLock)
            {
                bwp = new BufferedWaveProvider(wi.WaveFormat);
                bwp.BufferLength = BUFFERSIZE * 2;
                bwp.DiscardOnBufferOverflow = true;
            }
            try
            {
                wi.StartRecording();
            }
            catch
            {
                string msg = "Could not record from audio device!\n\n";
                msg += "Is your microphone plugged in?\n";
                msg += "Is it set as your default recording device?";
                MessageBox.Show(msg, "ERROR");
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {

            // plot the Xs and Ys for both graphs

            timer1.Stop();

            audio_prossing();
            PlotLatestData();

            int sec = (bwp == null) ? 256 : ((int)bwp.BufferedDuration.TotalMilliseconds)/8;
            
            int BufferedDuration = ((int)bwp.BufferedDuration.TotalMilliseconds);
            int BufferDuration = ((int)bwp.BufferDuration.TotalMilliseconds);
           
            timer1.Interval = (sec == 0)? (BufferDuration/2) : sec;
            
            timer1.Start();

        }
        
        public int numberOfDraws = 0;
        public bool needsAutoScaling = true;

        public async void PlotLatestData()
        {

            // check the incoming microphone audio
            int frameSize = BUFFERSIZE;
            var audioBytes = new byte[frameSize];
            if (bwp == null)
            {
                Console.WriteLine("------- return plotlatestdata ------ \n");
                return;

            }
            lock (balanceLock)
            {
                int count=bwp.Read(audioBytes, 0, frameSize);
            }
            
            // return if there's nothing new to plot
            if (audioBytes.Length == 0)
                return;
            if (audioBytes[frameSize - 2] == 0)
                return;

            // incoming data is 16-bit (2 bytes per audio point)
            int BYTES_PER_POINT = 2;//2

            // create a (32-bit) int array ready to fill with the 16-bit data
            int graphPointCount = audioBytes.Length / BYTES_PER_POINT;

            // create double arrays to hold the data we will graph
            double[] pcm = new double[graphPointCount];
            double[] fft = new double[graphPointCount];
            double[] fftReal = new double[graphPointCount / 2];
            short[] audio_in = new short[graphPointCount];
            
            double[] audio_in_plot = new double[graphPointCount];
            double[] audio_in_plot_half = new double[graphPointCount/2];
            double[] tones = new double[16];
            int[] idx = new int[16] { 160, 256, 352, 512, 640, 768, 864, 1024, 1152, 1280, 1440, 1536, 1664, 1792, 1888, 1984 };
            double[] indx = new double[graphPointCount];
            double[] selected_freq = new double[4096];

            int sum_z = 0;

            // populate Xs and Ys with double data
            for (int i = 0; i < graphPointCount; i++)
            {
                // read the int16 from the two bytes
                Int16 val = BitConverter.ToInt16(audioBytes, i*2 );
                indx[i] = i;
                
                if (val == 0)
                {
                    sum_z++;
                }
                // store the value in Ys as a percent (+/- 100% = 200%)
                pcm[i] = (double)(val) / Math.Pow(2, 16) * 200.0;
                

            }
            if (sum_z > graphPointCount / 2)
            {

                Array.Clear(pcm, 0, pcm.Length);

            }
            else
            {
                for (int i = 0; i < graphPointCount ; i++)
                {
                    Int16 val_detect = BitConverter.ToInt16(audioBytes, i*2);//i*2
                    audio_in[i] = (short)(val_detect);
                    
                }

                My_fft(audio_in, audio_in_plot);
             
            }

            // calculate the full FFT

            fft = FFT(pcm);   

            // determine horizontal axis units for graphs
            double pcmPointSpacingMs = RATE / 1000;
            double fftMaxFreq = RATE / 2;
            double fftPointSpacingHz =  (double)graphPointCount/RATE;
            
            // just keep the real half (the other half imaginary)
            Array.Copy(fft, fftReal, fftReal.Length);
            Array.Copy(audio_in_plot, audio_in_plot_half, audio_in_plot_half.Length);

            // plot the Xs and Ys for both graphs

            formsPlot1.Plot.Clear();
            formsPlot1.Plot.PlotSignal(pcm, pcmPointSpacingMs, 0, 0, Color.ForestGreen, 1, 5, null, null, null, null, lineStyle: LineStyle.Solid, true);
            
            
            formsPlot1.Refresh();

            formsPlot2.Plot.Clear();
           
            formsPlot2.Plot.PlotSignal(audio_in_plot_half, fftPointSpacingHz, 0, 0, Color.ForestGreen, 1, 5, null, null, null, null, lineStyle: LineStyle.Solid, true);
            formsPlot2.Refresh();

            int detector = detect_cs(fftReal);
            

           // Console.WriteLine("symbol from C#: {0} \n", detector);


            // optionally adjust the scale to automatically fit the data
            if (needsAutoScaling)
            {
                formsPlot1.AutoSize = true;
                formsPlot2.AutoSize = true;
                needsAutoScaling = false;
            }



            numberOfDraws += 1;
           


            // this reduces flicker and helps keep the program responsive
            Application.DoEvents();

        }
        private void bubbleSort(double[] arr)
        {
            double temp;
            for (int j = 0; j <= arr.Length - 2; j++)
            {
                for (int i = 0; i <= arr.Length - 2; i++)
                {
                    if (arr[i] > arr[i + 1])
                    {
                        temp = arr[i + 1];
                        arr[i + 1] = arr[i];
                        arr[i] = temp;
                    }
                }
            }
        }

        private void bubbleSort2(double[] audio_in, double[] idx)
        {
            double temp_array = 0;
            double temp_idx = 0;
            for (int i = audio_in.Length-1; i >= 0; i--)
                for (int j = audio_in.Length-1; j >= audio_in.Length - i; j--)
                    if (audio_in[j] > audio_in[j - 1])
                    {
                        temp_array = audio_in[j];
                        audio_in[j] = audio_in[j - 1];
                        audio_in[j - 1] = temp_array;

                        temp_idx = idx[j];
                        idx[j] = idx[j - 1];
                        idx[j - 1] = temp_idx;


                    }
        }

        
        private int detect_cs(double[] fftReal)
        {
            double[] tones = new double[DETECTED_TONES];
            double[] inx_arr= new double[fftReal.Length];
            double[] amp = new double[fftReal.Length];
            int[] indx = new int[]{ 80, 128, 176, 256, 320, 384, 432, 512, 576, 640, 720, 768, 832, 896, 944, 992 };

            for (int j = 0; j < fftReal.Length; j++)
            {
                inx_arr[j] = j;
               
            }
            for (int i = 0; i < 16; i++)
            {
                amp[indx[i]*2] = fftReal[indx[i]*2];
            }


            bubbleSort2(amp, inx_arr);

                   
           
            tones[0] = inx_arr[0] * ((double)16000 / (double)4096);
            tones[1] = inx_arr[1] * ((double)16000 / (double)4096);
            tones[2] = inx_arr[2] * ((double)16000 / (double)4096);
           
            

            bubbleSort(tones);
            
            return (int)find_symbol(tones);

        }

        public void audio_prossing()
        {

            Console.WriteLine("------- audio prossing ------ \n");
            // check the incoming microphone audio
            int frameSize = BUFFERSIZE;
            var audioBytes = new byte[frameSize];
            if (bwp == null)
            {
                Console.WriteLine("------- return audio prossing ------ \n");
                return;
            }

                bwp.Read(audioBytes, 0, frameSize);



            // return if there's nothing new to plot
            if (audioBytes.Length == 0) {
                Console.WriteLine("------- a.p exit 1 ------ \n");
                return;
            }
         
            // incoming data is 16-bit (2 bytes per audio point)
            int BYTES_PER_POINT = 2;
             //vars
             int graphPointCount = audioBytes.Length / BYTES_PER_POINT;
             short[] audio_in = new short[graphPointCount];
            
             //read buffer to audio_in
            for (int i = 0; i < graphPointCount; i++)
            {
                 Int16 val_detect = BitConverter.ToInt16(audioBytes, i*2 );//i*2
                 audio_in[i] = (short)(val_detect);
            
            }

            string s_audio_in = string.Join(" ,", audio_in);
            
            File.AppendAllText("buf_audio_in.txt", "[");
            File.AppendAllText("buf_audio_in.txt", s_audio_in);
            File.AppendAllText("buf_audio_in.txt", "];\n");
            File.AppendAllText("buf_audio_in.txt", "~~~end~~~\n");

            File.AppendAllText("buf_audio_in.csv", "\n");
            File.AppendAllText("buf_audio_in.csv", s_audio_in);

           

            char detect_c = sliding_windows_char(audio_in, audio_in.Length);

            Console.WriteLine("------- start prossing ------ \n");
            Console.WriteLine("symbol from cpp: {0} \n", detect_c);
            Console.WriteLine("------- end prossing ------ \n");

            string s_detect = string.Join(" ,", detect_c);
            File.AppendAllText("buf_audio_in.txt", "\nsymbol from cpp:");
            File.AppendAllText("buf_audio_in.txt", s_detect);
            File.AppendAllText("buf_audio_in.txt", "\n");

       
            for(int i = 0; i < 16; i++)
            {
                if(detect_c == symbol_chars[i])
                {
                    pin[pos_in_pin] = detect_c;
                    prev_symbol = detect_c;
                    pin_int[pos_in_pin] = i;
                    x_counter = 0;
                    break;
                }
            }
            
            if(detect_c == symbol_chars[16] && prev_symbol != symbol_chars[16])
            {
                pos_in_pin++;
                prev_symbol = detect_c;
               

            }
            else if(detect_c == 'X')
            {
                x_counter++;
                if(x_counter == 16)
                {
                    x_counter = 0;
                    pos_in_pin = 0;
                    pin[0] = '?';
                    pin[1] = '?';
                    pin[2] = '?';
                    pin[3] = '?';
                }
            }
            if (pos_in_pin >= 4)
            {
                pos_in_pin = 0;
                
                decode_shell(pin_int, decoded_pin_before, 0);
                decode_shell(pin_int, decoded_pin, 1);
                decode_shell(pin_int, decoded_pin_after, 2);
                Console.WriteLine("------- pin is: ------ \n");
                string s_pin_before = string.Join("", decoded_pin_before);
                string s_pin = string.Join("", decoded_pin);
                string s_pin_after = string.Join("", decoded_pin_after);
                Console.WriteLine("[{0}] \n", string.Join(", ", decoded_pin));

                if ((s_pin == user_pin_enter) || (s_pin_after == user_pin_enter) || (s_pin_before == user_pin_enter))
                {
                    x_counter = 0;
                    unlocking();
                    delay_lock_flag = false;
                }
                else if ((s_pin == lock_pin) || (s_pin_after == lock_pin) || (s_pin_before == lock_pin))
                {
                    x_counter = 0;
                    locking();
                    delay_lock_flag = false;
                }
                else if (((s_pin == delay_lock_pin) || (s_pin_after == delay_lock_pin) || (s_pin_before == delay_lock_pin)) && (!delay_lock_flag))
                {
                    Console.WriteLine("*********************delayyyyyyyyy*****\n");
                   
                    delay_lock_flag = true;
                    x_counter = 0;
                    timer2.Enabled = true;
                    timer2.Start();



                }
                else if (((s_pin == delay_lock_pin) || (s_pin_after == delay_lock_pin) || (s_pin_before == delay_lock_pin)) && (delay_lock_flag))
                {
                    
                    Console.WriteLine("*********************delay flag : {0} *******\n ", delay_lock_flag);
                    x_counter = 0;
                    Console.WriteLine("*********************delay stoped at : {0} *******\n ", timer2.ToString());
                    timer2.Stop();
                    timer2.Enabled = false;
                    delay_lock_flag = false;
                }
                
                else
                {
                    x_counter = 0;
                    Console.WriteLine("incorrect pin \n");
                    
                }
               
                Console.WriteLine("*********************pos in pin ={0}*****\n", pos_in_pin);
                Console.WriteLine("[{0}] \n", string.Join(", ", pin));
                label2.Text = string.Join(", ", pin);
                label2.Refresh();
                pin[0] = '?';
                pin[1] = '?';
                pin[2] = '?';
                pin[3] = '?';

                Console.WriteLine("*********************delay flag ={0}*****\n", delay_lock_flag);
                Console.WriteLine("*********************delay flag from elsesss ={0}*****\n", delay_lock_flag);
            }

            Console.WriteLine("*********************pos in pin ={0}*****\n",pos_in_pin);
            Console.WriteLine("[{0}] \n", string.Join(", ", pin));
            label2.Text = string.Join(", ", pin);
            label2.Refresh();
            string s_pin_etr = string.Join(" ,", pin);
            string s_pos = string.Join(" ,", pos_in_pin);
            File.AppendAllText("buf_audio_in.txt", "\n ******pin:");
            File.AppendAllText("buf_audio_in.txt", s_pin_etr);
            File.AppendAllText("buf_audio_in.txt", "********\n");
            File.AppendAllText("buf_audio_in.txt", "\n ******pos in pin:");
            File.AppendAllText("buf_audio_in.txt", s_pos);
            File.AppendAllText("buf_audio_in.txt", "********\n");
        }


        private void AutoScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            needsAutoScaling = true;
        }

        private void infoMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "";
            msg += "left-click-drag to pan\n";
            msg += "right-click-drag to zoom\n";
            msg += "middle-click to auto-axis\n";
            msg += "double-click for graphing stats\n";
            MessageBox.Show(msg);
        }
        
        private void websiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/swharden/Csharp-Data-Visualization");
        }
        
        public double[] FFT(double[] data)
        {
            double[] fft = new double[data.Length];
            Complex[] fftComplex = new Complex[data.Length];
            for (int i = 0; i < data.Length; i++)
                fftComplex[i] = new System.Numerics.Complex(data[i], 0.0);
            Accord.Math.FourierTransform.FFT(fftComplex, Accord.Math.FourierTransform.Direction.Forward);
            for (int i = 0; i < data.Length; i++)
                fft[i] = fftComplex[i].Magnitude;
            return fft;
        }
        
        private void formsPlot1_Load(object sender, EventArgs e)
        {

        }

        private void formsPlot2_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void timer2_Tick_1(object sender, EventArgs e)
        {
            Console.WriteLine("*********************delay flag from timer ={0}*****\n", delay_lock_flag);
            
            if (delay_lock_flag)
            {
                delay_lock_flag = false;
               
                locking();
                timer2.Stop();
                
                Console.WriteLine("########### timer 2 stoped  ########## \n");
                timer2.Enabled = false;
            }
            
            return;

        }
        
     
        private void label2_Click(object sender, EventArgs e)
        {

        }
        
        private void enter_Click(object sender, EventArgs e)
        {
            enter_msg.Text = "Password Recieved";
            label4.Text = "";
            label1.Text = "Press 'Open Workstation' on Smartphone to release the lock screen";
            
            for (int i = 0; i < 4; i++)
            {
                lockint[i] = Convert.ToInt32(user_pin[i] - '0');
                delay_lockint[i] = lockint[i] + 6;
                lockint[i] = 9 - lockint[i];

            }
            if (user_pin == "helo")
            {
                unlocking();
                return;
            }
            user_pin_enter = user_pin;
            lock_pin = string.Join("", lockint); 
            delay_lock_pin = string.Join("", delay_lockint);



        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }

}


