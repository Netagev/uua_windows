#ifndef FFT_H
#define FFT_H

#define FFT4K           0
#define FFT64K          1

void fft_init(int fftsize);
void gpfft(double Y[][2], int fftind);
void gpfft_dual(double Y1[][2], double Y2[][2]);
void ifft(double Y[][2], int fftind);

#endif //FFT_H

                                         

