//
// Created by Ofir Ben Yosef on 08/09/2021.
//
#include "stdio.h"
#include "signal-processing.h"
#include <iostream>
#include <math.h>
#include <signal.h>
#include <jni.h>
#include "fft.h"
#include "parameters.h"
#include "symbols_t.h"

using namespace std;


void copy_array(short* in, short* out, int length) {
    double tones[4] = {0};
    for (int j = 0; j < length; j++) {
        out[j] = in[j];
    }
}


int copy_array2(short* in, short* out, int length) {
    double tones[3] = {0};
    double SA[FFT_SIZE][2];
    short SSA[BLOCK_SIZE * 2] = {0};
    double amp[FFT_SIZE] = {0};

    double inx_arr[FFT_SIZE] = {0};
    int idx[16] = {160, 256, 352, 512, 640, 768, 864, 1024, 1152, 1280, 1440, 1536, 1664, 1792,
                   1888};
//singen_from_table(SSA, 3250, 16000, 2 * BLOCK_SIZE);
//int k=2;
    for (int j = 0; j < FFT_SIZE; j++) {
// SA[j][0] = (double)symbol_T[k][2 * j];
//SA[j][0] = (double)symbol_T[k][2 * j];
        SA[j][0] = (double) in[j];
        SA[j][1] = 0;
        inx_arr[j] = (double) j;
    }
    gpfft(SA, 0);
/* for(int i=0;i<FFT_SIZE/2;i++){
     if((i*SAMPLING_FREQ/FFT_SIZE)%1==0){
         amp[i] = SA[i][0]*SA[i][0] + SA[i][1]*SA[i][1];
     }
 }*/
    for (int i = 0; i < 16; i++) {
        amp[idx[i]] = SA[idx[i]][0] * SA[idx[i]][0] + SA[idx[i]][1] * SA[idx[i]][1];
    }
    bubbleSort2(amp, inx_arr, FFT_SIZE);
    if (!far_next_freq(amp)) {
        return -1;
    }
    tones[0] = inx_arr[0]*SAMPLING_FREQ/FFT_SIZE;
    tones[1] = inx_arr[1]*SAMPLING_FREQ/FFT_SIZE;
    tones[2] = inx_arr[2]*SAMPLING_FREQ/FFT_SIZE;
/*int curr_inx = 0;
double max = 0;
double freq;
for (int j = 0; j < DETECTED_TONES; j++) {
        max = 0;
        for (int i = 0; i < FFT_SIZE / 2; i++) {
            if ((max < amp[i]) && (amp[i] > 0.1)) {
                max = amp[i];
                freq = i * SAMPLING_FREQ / FFT_SIZE;
                tones[j] = freq;
                curr_inx = i;
            } else {
                max = max;
            }
        }
        amp[curr_inx] = 0;
}
*/
    bubbleSort(tones, DETECTED_TONES);
    return (int) find_symbol(tones);
}

void singen1(short l[], short r[], short sin_L, short sin_R, float fs) {
    short i=0;
    float stepL=0, stepR=0;
    static float phaseL=0.0, phaseR=0.0;
    short len = 8192;

    if(fs==0){
        for(i=0;i<len;i++){
            l[i] =0;
        }
    }
    else {

        stepL = ((float) sin_L) / fs;
        stepR = ((float) sin_R) / fs;

        for (i = 0; i < len / 2; i++) {
            l[2 * i] = (short) (Q14 * sin(phaseL));
            l[2 * i + 1] = (short) (Q14 * sin(phaseR + PI /
                                                       2));  //change to sin(phaseR+PI/2)) to creat a circle

            phaseL += stepL * 2 * PI;
            if (phaseL > (2 * PI)) phaseL -= 2 * PI;
            phaseR += stepR * 2 * PI;
            if (phaseR > (2 * PI)) phaseR -= 2 * PI;
        }
    }
}

void singen_from_table(short out[],short freq, float Fs, int length) {
    int i;
    static short step, jump=0;
    step=freq;
//int length = 8192;
    for (i=0; i<(length/2);i++){

        jump= jump + step;

        if(jump>=Fs) jump=jump-Fs;
        out[2*i+1] = sintbl[jump/100];
        out[2*i] = sintbl[jump/100];
    }
}
void gen_symbol(short out[],short symbol, int length) {
    if(symbol<0 || symbol>10){
        for(int i=0;i<length;i++) {
            out[i] = 0;
            return;
        }
    }

    for(int i=0;i<length;i++) {
        out[i] = symbol_Table[symbol][i];
    }
}
void tone_detection(short adin[2 * BLOCK_SIZE], double tones[], int slice) {
/* @brief    finds the tones whith the largest amplitude within a given slice
   @param    adin[]: the audio input
             tones[]: the detected tones from the slice
             slice: the part of the input audio array we detect from

*/
    int j, i,k, s=0;
    double amplitude[FFT_SIZE], tones_amp[DETECTED_TONES] = { 0 };
    double temp = 0, tempidx = 0;

    double SA[FFT_SIZE][2] = { 0 };

    for (j = 0; j < FFT_SIZE; j++) {
        SA[j][0] = ((double) adin[2 * j]);
        SA[j][1] = 0;
    }

    gpfft(SA, 0);



    for (i = 0; i < DETECTED_TONES; i++) {
        tones[i] = (double )END_TONE*SAMPLING_FREQ / 4*FFT_SIZE;
    }
    for (k = START_TONE/JUMP; k < START_TONE/JUMP+(END_TONE-START_TONE)/(DETECTED_TONES*JUMP) ; k++) {

        for (i = 0; i < DETECTED_TONES; i++) {
            j=(k+i*DETECTED_TONES)*JUMP;
            amplitude[j] = SA[j][0] * SA[j][0] + SA[j][1] * SA[j][1];
            if ((amplitude[j] > tones_amp[i])&& (amplitude[j] > 0.01)){
                tones_amp[i] = amplitude[j];
                tones[i] = (double)j * SAMPLING_FREQ / FFT_SIZE;
//printf("\n tones[%d]=%f" ,i,tones[i] );
            }
        }
    }

}

void swap(double *xp, double *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(double arr[], int n)
{
    int i, j;
    for (i = 0; i < n-1; i++)

// Last i elements are already in place
        for (j = 0; j < n-i-1; j++)
            if (arr[j] > arr[j+1])
                swap(&arr[j], &arr[j+1]);
}

int find_symbol(double arr[]){
/*
 *@ param : arr[] - array of detected tones
 * if find symbol that fit return the symbol
 * else return -1
 */
    for(int i = 0;i<11;i++){
        if((arr[0]==symbol_freq_T[i][0])&&(arr[1]==symbol_freq_T[i][1])&&(arr[2]==symbol_freq_T[i][2])){
            return i;
        }
    }
    return -1;
}

void getPin(int arr[], int length, int txt){
    int mod = 10;
    int temp_arr[10] = {-1};
    for(int i = 0;i<length;i++){
        temp_arr[i] = txt%mod;
        txt = txt - temp_arr[i];
        txt = txt/mod ;
        arr[length-i-1] = temp_arr[i];

    }
}
bool far_next_freq(double arr[]){
/*
     * check if the next freq is smaller
     */
    int k = DETECTED_TONES;
    if(arr[k-1]>3*arr[k]){
        return true;
    }
    return false;
}
void bubbleSort2(double arr1[], double arr2[], int n){
/*
     * sort an array in descending order and index array at the same order
     */
    for (int i = n; i >= 0; i--)
        for (int j = n; j > n - i; j--)
            if (arr1[j]  > arr1[j-1]){
                swap(&arr1[j], &arr1[j - 1]);//main array
                swap(&arr2[j], &arr2[j - 1]);//index array
            }
}
void push_array(short arr1[],short arr2[]){
/*
 * arr1[] - new array size of FFT_SIZE
 * arr2[] old array size of 2*FFT_SIZE
 * */
    short temp = 0;
    for(int i=0;i<FFT_SIZE;i++){
        temp = arr2[i + FFT_SIZE];
        arr2[i + FFT_SIZE] = arr1[i];
        arr2[i] = temp;
    }
}
int majority_vote(int symbols[],int len){
    int count[10] = { 0 };
    for(int i =0;i<len;i++){
        if(symbols[i]==-1){
            continue;
        }
        else{
            count[symbols[i]]++;
        }
    }
    for(int i =0;i<10;i++){
        if(count[i] >2){
            return i;
        }
    }
    return -1;
}
int sliding_windows(short* in, short* out, int length){
    static short buffer[2*FFT_SIZE] = { 0 };
    short SSA[FFT_SIZE] = {0};
    for (int j = 0; j < FFT_SIZE; j++) {
        SSA[j] = in[2 * j];
//SSA[j] = symbol_T[8][2 * j];
    }
    int k = 0;
    push_array(SSA,buffer);
    int add = FFT_SIZE/4;//1024
    int the_symbols[4] = { 0 };
    for(int i = 0;i<FFT_SIZE;i=i+add){
        for(int j=0;j<FFT_SIZE;j++){
            SSA[j] = buffer[j + i];
        }
        the_symbols[k] = copy_array2(SSA,out, FFT_SIZE);
        k++;
    }
    return majority_vote(the_symbols,4);
}
char* find_full_pin(short* in, short* out, int length){
    static int i=0;
// char *not_ready = "not ready";
    static char the_pin[4] ={'0','0','0','0'};
    static char the_pin_temp[14] ={'0'};
    the_pin_temp[i] = (char)sliding_windows(in,out,length);
//the_pin[i] = (char)copy_array2(in,out,length);

    if(i<13){
        i++;
        return "not ready";
    }
    else if(i==13){
        i=0;
        for(int j=1;j<14;j=j+2) {
            if ((the_pin_temp[j] == 10)) {
                continue;
            }
            else {
                if (the_pin_temp[j] != '-1') {
                    the_pin[j / 4] = the_pin_temp[j];
                }
                else if (the_pin_temp[j - 1] != '-1') {
                    the_pin[j / 4] = the_pin_temp[j - 1];
                }
            }
        }
        return the_pin;
    }
    return "not ready";
}
void test(short *in,short *out, int length){
    short temp[2*FFT_SIZE] = {0};
    short pin[14] ={1,1,10,10,0,0,10,10,5,5,10,10,8,8};
    char *str = NULL;
    for(int i=0;i<14;i++){

        gen_symbol(temp,pin[i],2*FFT_SIZE);
        str = find_full_pin(temp,out,FFT_SIZE);
    }
}