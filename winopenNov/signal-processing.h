//
// Created by Ofir Ben Yosef on 08/09/2021.
//

#ifndef MY_APPLICATION_SIGNAL_PROCESSING_H
#define MY_APPLICATION_SIGNAL_PROCESSING_H

#include <jni.h>
#include "parameters.h"
#include <string.h>
#include "symbols_t.h"
// functions :
void bubbleSort(double arr[], int n);
void swap(double *xp, double *yp);
int copy_array2(short* in, short* out, int length);
void singen1(short l[], short r[], short sin_L, short sin_R, float fs);
void copy_array(short* in, short* out, int length);
void tone_detection(short adin[2 * BLOCK_SIZE], double tones[], int slice);
int find_symbol(double arr[]);
void bubbleSort2(double arr1[], double arr2[], int n);
bool far_next_freq(double arr[]);
void getPin(int arr[], int length, int txt);
void push_array(short arr1[],short arr2[]);
int majority_vote(char symbols[],int len);
int sliding_windows(short* in, short* out, int length);
char* find_full_pin(short* in, short* out, int length);
void gen_symbol(short out[],short symbol, int length);
void test(short *in,short *out, int length);
#endif //MY_APPLICATION_SIGNAL_PROCESSING_H
