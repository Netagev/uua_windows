//
// Created by Ofir Ben Yosef on 15/12/2021.
//

#ifndef UUA_SYMBOLS_T_H
#define UUA_SYMBOLS_T_H
#include "parameters.h"
extern short symbol_Table[11][2*FFT_SIZE];
extern short symbol_freq_T[11][3];
extern short sintbl[160];

#endif //UUA_SYMBOLS_T_H
